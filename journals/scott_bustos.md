## 02/09/2023
We worked on finising the wireframing. We also fixed some api-design issues.

## 02/10/2023
We reviewed the documentation for FASTapi. We also went through the learn and watched curtis's video

## 02/13/2023
Our group worked on creating a database for our POSTgreSQL. We also created the migrations and worked on the CRUD for our FASTapi.
We went back through the project refactored our code as well.

## 02/14/2023
Worked on creating the database. Our team also fixed the authentication token. We updated our queries folder with new function parameters.
Also debugged issues throughout the day that was discovered.

## 02/15/2023
We did pair programming today. Kyle and Malcolm worked on creating comments while maddy and I worked on the recommedations api and the likes.

## 02/16/2023
We took a step back today and studied redux and worked on coming up with how we are going to design the front end. We decided to use tailwind
for the front end design.

## 02/21/2023
We started working with redux and created forms and our store. Today we were mostly testing and learning while setting everything up.

## 02/22/2023
We worked on finshing our endpoints for the user authentication. We then moved on to creating wine end points.

## 02/23/2023
We finshed the wine api endpoints and started working with the like endpoints. We created a card for our wines so we could test the likes on it.

## 02/24/2023
We tested out redux and how their slices work. We also added more styling to the homepage.

## 02/27/2023
Styled create the wine page using react. We also worked on making a blur for the wine-page.

## 02/28/2023
Worked on CRUD in redux. We also worked on fixing some of the bugs we had discovered.

## 03/01/2023
We worked on creating comments on the frontend. We had to fix some functionality of it to work. We also worked on react-tailwind

## 03/02/2023
We made a recommadation redux list. We created logs. Continued to make changes to our css.

## 03/06/2023
Styled dark mode theme. We also deleted some features that were not in used. We added a websocket to our application as well.

## 03/07/2023
Worked on fixing the console errors. We also did somemore css styling and looked our sql.

## 3/08/2023
Worked on CICD do get our website deployed. We also worked on the flake8 errors.


## 3/09/2023
Successfully deployed our application and finshed the project. We also did some debugging for our errors.
